package banco.cuentas;


import java.util.Set;
public class CuentaCorriente extends Cuenta {
	
	private int saldoEnRojo;
	private int saldoEnRojoOriginal;
	
    public String toString() {
        return super.toString() + ",, " + "Saldo en rojo: " + saldoEnRojo;
    }
	
	// Constructor que permite crear la Cuenta Corriente, con un solo Titular
	public CuentaCorriente(String nroCuenta, Cliente _cliente, int saldoRojo){
		super( nroCuenta, _cliente);
		this.saldoEnRojo=saldoRojo;
		this.saldoEnRojoOriginal=saldoRojo;
		//Asigna al Cliente pasado por parametro, la CuentaCorriente actual
		_cliente.agregarCuentaCorriente(nroCuenta);
	}
	
	// Constructor que permite crear la Cuenta Corriente, con varios Titulares al mismo tiempo	
	CuentaCorriente(String nroCuenta, Set<Cliente> _clientes, int saldoRojo){
		super( nroCuenta, _clientes);
		this.saldoEnRojo=saldoRojo;
		this.saldoEnRojoOriginal=saldoRojo;
		// Itera para todos los clientes pasados por parametro
		for (Cliente _cliente: _clientes){
			//Asigna al Cliente _cliente, la CuentaCorriente actual
			_cliente.agregarCuentaCorriente(nroCuenta);
		};
	}
	
	 public boolean equals(CuentaCorriente otra) {
		return ( super.equals(otra) && (this.saldoEnRojo==otra.saldoEnRojo));
	}
	 
	public void depositar(int valor){
		if((this.verSaldo()==0) && (saldoEnRojo<saldoEnRojoOriginal)){
		// Si entro es que se uso el saldo en rojo
			if(valor>saldoEnRojoOriginal){
			// Entonces el valor nos sirve para saldar el saldoenrojo y agregar al saldo
				saldoEnRojo=saldoEnRojoOriginal;
				valor-=saldoEnRojoOriginal;
				super.depositar(valor);
			}else{
			// Entonces el valor nos sirve solo para saldar el saldoenrojo 
				saldoEnRojo+=valor;
			}
		}else{
			super.depositar(valor);
		}
	} 

	public void extraer(int valor) throws SaldoInsuficienteException{
		// Permite extraer la suma indicada por valor
		if(this.verSaldo()==0){
			if((saldoEnRojo>0) && (valor<=saldoEnRojo)){
					// si pide mas plata que el saldo en rojo, no hace nada
					saldoEnRojo -= valor;				
			}else{
				throw new SaldoInsuficienteException();
			}
		}else{
//			if(0>valor && this.verSaldo()<=valor){
//				// si saca mas que el saldo saca lo que puede
//				super.extraer(valor);
//				
//			}else{
//				
//				throw new SaldoInsuficienteException();
//				
//			}
			super.extraer(valor);
		}	
	}	
	
	public int verSaldoEnRojo(){
		// retorna el saldo en rojo actual 
		return this.saldoEnRojo;
	} 
	
	public void agregarTitularCC(Cliente _cliente){
		// llama al metodo de la Clase Cuenta, para agregar el Cliente
		// pasado por parametro a los Titulares de la CuentaCorriente
		agregarTitular(_cliente);
		// agrega la Cuenta Corriente actual a las CuentasCorrientes del Cliente
		_cliente.agregarCuentaCorriente(this.getNroDeCuenta());
	}

	
	
}
