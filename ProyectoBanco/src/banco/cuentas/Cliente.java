package banco.cuentas;

import java.util.Set;
import java.util.HashSet;

public class Cliente{
	
	private String nombreTitular;
	private int dniTitular;
	private Set<String> _cuentasSueldo = new HashSet<String>();
	private Set<String> _cuentasCorriente = new HashSet<String>();
	
    public String toString() {
        return "Nombre del titular: " + nombreTitular + ",, "
                + "DNI del titular: " + dniTitular + ",, "
        		+ "Cuentas sueldo: " + _cuentasSueldo.toString() + ",, " 
                + "Cuentas corrientes: " + _cuentasCorriente.toString();
    }
    
	// Constructor vacio
	Cliente(){};
	
	// Contructor para instanciar un Cliente en base a su nombre y dni
	Cliente(String nomTitular, int dniTitular){
		this.nombreTitular = nomTitular;
		this.dniTitular = dniTitular;
	}
	
	public String getNombreTitular(){
		return nombreTitular;
	}

	public int getDniTitular(){
		return dniTitular;
	}
	
	public Set<String> getCuentasSueldo(){
		return _cuentasSueldo;
	}
	
	public Set<String> getCuentasCorrientes(){
		return _cuentasCorriente;
	}

	
	public void agregarCuentaSueldo(String cs){
		// No puede pasar que tenga, mayor cantidad, de cuentas sueldo que cuentas corrientes
		if(_cuentasSueldo.size()<=_cuentasCorriente.size()){
			_cuentasSueldo.add(cs);
		}
	}
	
	public void agregarCuentaCorriente(String cc){
		_cuentasCorriente.add(cc);
	}
	
    public boolean contains(Set<Cliente> c) {
    	boolean valorDeVerdad=false;
        for (Cliente element : c) {
        	valorDeVerdad=valorDeVerdad||(this.equals(element));
        }
        return valorDeVerdad;
    }

	 public boolean equals(Object otra) {
		 if(this == otra){
			 return true;
		 }
		 if(!(otra instanceof Cliente)){
			 return false;
		 }
		 return ((this.nombreTitular==((Cliente)otra).nombreTitular) 
				&& (this.dniTitular==((Cliente)otra).dniTitular)
				&& ((this._cuentasSueldo.size()==((Cliente)otra)._cuentasSueldo.size())&&this._cuentasSueldo.containsAll(((Cliente)otra)._cuentasSueldo))
				&& ((this._cuentasCorriente.size()==((Cliente)otra)._cuentasCorriente.size())&&this._cuentasCorriente.containsAll(((Cliente)otra)._cuentasCorriente)));
	}
	 
}
