package banco.cuentas;

import java.util.HashSet;
import java.util.Set;

abstract class Cuenta {
	
	private String numeroDeCuenta;
	private int saldo;
	private Set<Cliente> titulares = new HashSet<Cliente>();
	
    public String toString() {
        return "Numero de cuenta: " + numeroDeCuenta + ",, "
                + "Saldo: " + saldo + ",, "
        		+ "Titulares: " + titulares.toString();
    }
	
	// Contructor vacio
	public Cuenta(){}
	
	// Constructor que permite crear la cuenta, con un solo Titular
	public Cuenta(String nroCuenta, Cliente _cliente){
		this.numeroDeCuenta=nroCuenta;
		this.titulares.add(_cliente);//agregarTitular(_cliente);
	}
	
	// Constructor que permite crear la cuenta, con varios Titulares al mismo tiempo
	public Cuenta(String nroCuenta, Set<Cliente> _clientes){
		this.numeroDeCuenta=nroCuenta;
		this.titulares=_clientes;
	}
	
	public void depositar(int valor){
		// Permite depositar la suma indicada por valor
		this.saldo+=valor;
	}
	
	public void extraer(int valor) throws SaldoInsuficienteException{
		// Permite extraer la suma indicada por valor
		if(this.saldo>0 && this.saldo <=valor ){
			this.saldo-=valor;			
		}else{
			throw new SaldoInsuficienteException();
		}
	}
	
	public int verSaldo(){
		// Retorna el saldo actual
		return this.saldo;
	}
	
	public String getNroDeCuenta(){
		// Retorna el numero de cuenta
		return this.numeroDeCuenta;
	}
	

	public void agregarTitular(Cliente _cliente){
		// Permite agregar un Cliente al conjunto actual de titulares
		this.titulares.add(_cliente);
		_cliente.agregarCuentaCorriente(this.numeroDeCuenta);
	}
	
	
	 public boolean equals(Object otra) {
		 if(this == otra){
			 return true;
		 }
		 if(!(otra instanceof Cuenta)){
			 return false;
		 }
		 return ((this.numeroDeCuenta==((Cuenta)otra).numeroDeCuenta) 
						&& (this.saldo==((Cuenta)otra).saldo)
						&& (mismosClientes(this.titulares,((Cuenta)otra).titulares)));			 
		 
	}
	 
	private boolean mismosClientes(Set<Cliente> c1, Set<Cliente> c2){
		if(c1.size()==c2.size()){
			for (Cliente element : c1) {
	        	if(!(element.contains(c2))){
	        		return false;
	        	}	
	        }
	        return true;			
		}else{
			return false;
		}
	}


}
