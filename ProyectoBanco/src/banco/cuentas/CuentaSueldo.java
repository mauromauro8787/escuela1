package banco.cuentas;


public class CuentaSueldo extends Cuenta {
		private String nombreDeEmpleador;
		
		// Constructor de la clase CuentaSueldo
	   public CuentaSueldo(String nroCuenta, Cliente _cliente, String nombreEmpleador){
			super( nroCuenta, _cliente);
			this.nombreDeEmpleador= nombreEmpleador;
			// agregar la Cuenta Corriente actual a las Cuentas Corrientes del Cliente
			_cliente.agregarCuentaSueldo(nroCuenta);
		}
	   
	    public String toString() {
	        return super.toString();
	    }
	    
       public boolean equals(CuentaSueldo c){
		   return super.equals((Cuenta)c);
	   }

}
