package banco.cuentas;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.HashSet;

import org.junit.Test;


public class cuentaSueldoTest {

	@Test
	public void testAndaConstructor() throws SaldoInsuficienteException{
		
		Cliente cliente1 = new Cliente("Nombre1",1);
		Cliente cliente2 = new Cliente("Nombre2",2);
		
		Cliente cliente3 = new Cliente("Nombre1",1);
		Cliente cliente4 = new Cliente("Nombre2",2);
		
		Set<Cliente> cj1 = new HashSet<Cliente>();
		cj1.add(cliente1);
		cj1.add(cliente2);
		Set<Cliente> cj2 = new HashSet<Cliente>();
		cj2.add(cliente3);
		cj2.add(cliente4);
		
		CuentaSueldo c1 = new CuentaSueldo("#DeCuenta",cliente1,"Empleador");
		c1.depositar(1);
		c1.extraer(1);
		assertTrue("Error en VerSaldo",c1.verSaldo()==0);
		
		CuentaSueldo c2 = new CuentaSueldo("#DeCuenta",cliente1,"Empleador");
		c2.depositar(1);
		c2.extraer(1);
		assertTrue("Error en VerSaldo",c2.verSaldo()==0);
		assertTrue("Error En Equal",c1.equals(c2));
	}
	
}