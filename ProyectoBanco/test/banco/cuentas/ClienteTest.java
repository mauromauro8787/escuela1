package banco.cuentas;

import static org.junit.Assert.*;
import java.util.Set;
import java.util.HashSet;
import org.junit.Test;

public class ClienteTest {

	@Test
	public void testAndaConstructor() {
		
		Cliente c1=new Cliente("1",1);
		Cliente c2=new Cliente("2",2);
		Cliente c1bis=new Cliente("1",1);
		
		
		assertTrue("No Son iguales", !c1.equals(c2));
		assertTrue("Son iguales", c1.equals(c1bis));
		
		c1.agregarCuentaCorriente("nuevaCC");
		assertTrue("Son iguales", !c1.equals(c1bis));
		
		c1bis.agregarCuentaCorriente("nuevaCC");
		assertTrue("No Son iguales", c1.equals(c1bis));
		// aca con el equals version casera, nos informa que c1 y c1bis son iguales
		
		// Ahora armamos dos conjuntos, pero uno con c1 y otro con c1bis
		// para chequear que el containsAll mira el ID
		Set<Cliente> cjClientes = new HashSet<Cliente>();
		cjClientes.add(c1);
		cjClientes.add(c2);
		assertTrue("Son iguales", c1.contains(cjClientes));
		
		Set<Cliente> cjClientes2 = new HashSet<Cliente>();
		cjClientes2.add(c1bis);// deberia ser igual a c1 y el 
							   // containsAll deberia dar true
		cjClientes2.add(c2);
		assertTrue("Son iguales", !cjClientes.containsAll(cjClientes2));
		// creo que no funciona porque mira el ID
		
		System.out.println( c1.toString());

	}

}



