package banco.cuentas;

import java.lang.Exception;

public class SaldoInsuficienteException extends Exception{
	  
	public SaldoInsuficienteException() { super("Saldo insuficiente"); }


}
