package banco.cuentas;

import static org.junit.Assert.*;

import org.junit.Test;

public class cuentaTest {

	@Test
	public void testAndaConstructor() throws SaldoInsuficienteException{
		Cliente c1 = new Cliente("c1",1);
		//Chequeo el constructor del generador de un cliente
		CuentaSueldo c = new CuentaSueldo("NroDeCuenta1",c1,"Empleador");
		c.depositar(1);
		c.extraer(1);
		assertTrue("Error",c.verSaldo()==0);
		// pruebo la igualdad de dos cuentas con los mismos clientes de distintos ID		
		CuentaSueldo cbis = new CuentaSueldo("NroDeCuenta1",c1,"Empleador");
		c.depositar(1);
		c.extraer(1);
		assertTrue("Error",c.verSaldo()==0);
		assertTrue("Error",c.equals(cbis));
	}

}
