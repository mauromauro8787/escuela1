package banco.cuentas;
import java.util.Set;
import java.util.HashSet;

import static org.junit.Assert.*;

import org.junit.Test;


public class cuentaCorrienteTest {

	@Test
	public void testAndaConMismosClientes() throws SaldoInsuficienteException{
		Cliente c1 = new Cliente("c1",1);
		Cliente c2 = new Cliente("c2",2);
		Cliente c1bis = new Cliente("c1",1);
		Cliente c2bis = new Cliente("c2",2);
				
		int valorDelSaldoEnRojo=1;
		assertTrue("No son iguales al crearlos",c1.equals(c1bis));
		
		CuentaCorriente c = new CuentaCorriente("NroDeCuenta1",c1,valorDelSaldoEnRojo);
		c.depositar(1);
		c.extraer(1);
		assertTrue("Error",c.verSaldo()==0);
		// c1 fue modificado y se le agrego una cuentaCorriente que c1bis no tiene
		assertFalse("Si no dio false es que son iguales",c1.equals(c1bis));
		// hasta aca probe que dos clientes son iguales y si agrego creo una cuenta con uno
		// pasan a ser diferentes.
		
		
		Set<Cliente> cjClientes = new HashSet<Cliente>();
		cjClientes.add(c1bis);
		cjClientes.add(c2bis);
		// creo cc con c1bis y c2bis.
		CuentaCorriente cc = new CuentaCorriente("NroDeCuenta1",cjClientes,valorDelSaldoEnRojo);
		cc.depositar(1);
		cc.extraer(1);
		assertTrue("Error",cc.verSaldo()==0);

		// ahora las cuentas deben ser diferentes
		assertFalse("Error",c.equals(cc));		
		
		// c ya fue creada con c1, agrego como titular a c2
		c.agregarTitular(c2);		

		// ahora las cuentas deben ser iguales
		assertTrue("Error",c.equals(cc));		

	}	
	@Test(expected=SaldoInsuficienteException.class )
	public void testAndaConstructor() throws SaldoInsuficienteException{
		Cliente c1 = new Cliente("c1",1);
		Cliente c2 = new Cliente("c2",2);
		Cliente c3 = new Cliente("c3",3);
		Cliente c4 = new Cliente("c4",4);
		int valorDelSaldoEnRojo=1;
		CuentaCorriente c = new CuentaCorriente("NroDeCuenta1",c1,valorDelSaldoEnRojo);
		c.depositar(1);
		c.extraer(1);
		assertTrue("Error",c.verSaldo()==0);
		c.agregarTitular(c2);
		Set<Cliente> cjClientes = new HashSet<Cliente>();
		cjClientes.add(c1);
		cjClientes.add(c2);
		cjClientes.add(c3);
		CuentaCorriente cc = new CuentaCorriente("NroDeCuenta1",cjClientes,valorDelSaldoEnRojo);
		cc.depositar(1);
		cc.extraer(1);
		assertTrue("Error",cc.verSaldo()==0);
		cc.extraer(1);
		assertTrue("Error",cc.verSaldoEnRojo()==0);
		cc.extraer(1);
		assertTrue("Error",cc.verSaldoEnRojo()==0);
		assertTrue("Error",cc.verSaldo()==0);
		cc.depositar(2);
		assertTrue("Error",(cc.verSaldoEnRojo()==1) && (cc.verSaldo()==1));
		assertFalse("Error",c.equals(cc));
		c.agregarTitular(c4);
		assertFalse("Error",c.equals(cc));	
		System.out.println( c.toString());

	}
	
}